package newSrc;

public class LinkedList {
    Node first;
    Node last;

    public LinkedList() {
        this.first = null;
        this.last = null;
    }

    public Node getfirst() {
        return first;
    }

    public Node getLast() {
        return last;
    }

    public void addNodeLast(int data) {
        Node newNode = new Node(data);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            last.next = newNode;
            last = newNode;
        }
    }

    public void addNodeFirst(int data) {
        Node newNode = new Node(data);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            first.next = first;
            first = newNode;
        }
    }

    public void displayList() {
        Node current = first;
        while (current != null) {
            System.out.println(current.data);
            current = current.next;
        }
        System.out.println("That is all in LinkedList");
    }
}
